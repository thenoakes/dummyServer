var fs = require('fs');
var multiparty = require('multiparty');

/** Read a ReadableStream object representing plain text content */
function getContentFromReadableStream(stream, cb) {
    const chunks = [];
    stream.on('data', (chunk) => {
        chunks.push(chunk.toString());
    });
    stream.on('end', () => {
        cb(chunks.join(''));
    });
}


/** Read a text file on disk and pass the result to a callback */
function readTextFile(file, cb) {
    fs.readFile(file, 'utf8', function(err, data) {
        if (err) {
            console.error(err);
        }
        cb(data);
    });
}


/** A function which returns a callback that can be used to return the given XML content with a 200 response */
function returnXmlCallback(res) {
    return (xml) => res.status(200).type('application/xml').send(xml);
} 

// function echoMultipartPost(req, res, next) {
//     return echoMultipartPostWithCallback(req, res, next, () => res.sendStatus(200))
// }


// /** A function which takes an incoming POST request and prints it, raw, to the screen and to a file */
// function echoMultipartPostWithCallback(req, res, next, completionCallback) {

//     // A function for writing out to the console (optionally snipped) and to a new file
//     const echo = (() => { 

//         const log_file = fs.createWriteStream(`post${(new Date()).getTime()}.http`, {flags : 'w'});

//         return (output) => {

//             log_file.write(output + "\n");
//             const contentLength = output.length;
//             if (!snipLargeContent || contentLength <= maxLength) console.log(output);
//             else console.log(`${output.slice(0, maxLength / 2)} \n < ... snip ... > \n ${output.slice(contentLength - maxLength / 2, contentLength)}`);

//         }
//     })();

//     console.log("");
//     echo(`POST ${req.path} HTTP/1.1`);

//     // Parse the MIME boundary
//     const boundary = (() => {
//         const BOUNDARY_PARSER = /^multipart\/.+?(?:; boundary=(?:(?:"(.+)")|(?:([^\s]+))))$/i;
//         const parsed = BOUNDARY_PARSER.exec(req.headers['content-type']);
//         return parsed[1] || parsed[2];
//     })();

//     // Print the request headers
//     for (let header in req.headers) echo(header + ": " + req.headers[header]);
    
//     // Set up processing for the multipart content
//     var form = new multiparty.Form();
//     form.on('part', function (part) {
         
//         // Detect base64 encoding to print nicely to the console
//         const isBase64 = (() => {
//             const transferEncoding = part.headers[Object.keys(part.headers).find(key => key.toLowerCase() === "content-transfer-encoding")];
//             if (transferEncoding && transferEncoding.toLowerCase() == "base64") return true;
//             return false;
//         })();

//         if (isBase64) part.setEncoding('base64');

//         // Get the body content of the MIME part
//         getContentFromReadableStream(part, (content) => {

//             echo("\n--" + boundary);

//             // Print the part's headers
//             for (let header in part.headers) echo(header + ": " + part.headers[header]);
            
//             echo("");
//             echo(content);

//             // Continue with the next part
//             part.resume();

//         });

//         part.on('error', function (err) {
//             console.error(err);
//         });

//     }).on('close', function () {
//         echo("\n--" + boundary + "--");
//         // Send the response when reading the form has finished
//         completionCallback();
//     });

//     // Process the incoming request
//     form.parse(req);
// }

module.exports = {
    readTextFile,
    returnXmlCallback,
    // echoMultipartPost,
    // echoMultipartPostWithCallback,
    getContentFromReadableStream
};
