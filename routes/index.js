var fs = require('fs');
var express = require('express');
var router = express.Router();
var multiparty = require('multiparty');

const {
    readTextFile,
    returnXmlCallback,
    getContentFromReadableStream
} = require("./test-utilities");


// Simulate the first attachment being Large
let usedLarge = false;
let msgCounter = 0;
const junk = `QQ==`;

// An enumeration of possible testing scenarios
const MOCK_RPC_RESULT = Object.freeze({
    /** Return an extract with 1 large and 3 small attachments */
    returnExtract_1Large_3Small: 1,
    /** Return an extract with 100 small attachments */
    returnExtract_100Small: 2,
    /** Return an empty RPC wrapper which indicates an EHR import success */
    returnImportedExtract: 3,
    /** Return an extract with 4 small attachments */
    returnExtract_4Small: 4,
    /** Return an extract with 1 absent attachment */
    returnExtract_1Absent: 5,
    /** Return an extract with 1 large attachment */
    returnExtract_1Large: 6,
});

// ==== QUICK AND DIRTY CONFIGURATION ====

/** Should the output of large POSTs in the console be snipped (setting to false can cause performance issues for large payloads) */
const snipLargeContent = true;    
/** The number of characters to cap a large payload to, when snipping */
const maxLength = 10000;
/** Set the testing scenario to a value of the MOCK_RPC_RESULT enum */
const testingScenario = MOCK_RPC_RESULT.returnExtract_1Large;


/** A function which takes an incoming POST request and prints it, raw, to the screen and to a file */
function echoMultipartPost(req, res, next) {

    /** A function for writing out to the console (optionally snipped) and to a new file */
    const echo = (() => { 

        const log_file = fs.createWriteStream(`post${(new Date()).getTime()}.http`, {flags : 'w'});

        return (output) => {

            log_file.write(output + "\n");
            const contentLength = output.length;
            if (!snipLargeContent || contentLength <= maxLength) console.log(output);
            else console.log(`${output.slice(0, maxLength / 2)} \n < ... snip ... > \n ${output.slice(contentLength - maxLength / 2, contentLength)}`);

        }
    })();

    console.log("");
    echo(`POST ${req.path} HTTP/1.1`);

    // Print the request headers
    for (let header in req.headers) echo(header + ": " + req.headers[header]);

    // if (req.headers["Expect"] === "100 Continue") {
    //     return res.sendStatus(100);      
    // }

    // Attempt to parse the MIME boundary
    const boundary = (() => {
        //try {
            const BOUNDARY_PARSER = /;\s*boundary=(.*?)\s*(;|$)/i;
            const parsed = BOUNDARY_PARSER.exec(req.headers['content-type']);
            let boundary = (parsed && parsed.length) ? (parsed[1] || parsed[2]) : "";
            return boundary.replace(/\"/g, "");
        //}
        //catch {
            // If no boundary is detected, return an empty string denoting non-multipart
            //return;
        //}
    })();

    if (!boundary.length) {

        echo("");

        // If parser has produced an object => process for form-urlencoded
        if (typeof req.body === 'object') {
            let payload = '';
            for (let key in req.body) {
                payload += key + "=" + req.body[key] + "&"
            }
            payload = encodeURI(payload.substring(0, payload.length - 1));
            echo(payload);
        }
        // Otherwise
        else {
            echo (req.body);
        }

        res.sendStatus(200);
        return console.log('\n^^ NON-MULTIPART POST LOGGED @ ', new Date(), ' ^^\n');
    }

    // Boundary identified => process for multipart content
    var form = new multiparty.Form();
    form.on('part', function (part) {
         
        // Detect base64 encoding to print nicely to the console
        const isBase64 = (() => {
            const transferEncoding = part.headers[Object.keys(part.headers).find(key => key.toLowerCase() === "content-transfer-encoding")];
            if (transferEncoding && transferEncoding.toLowerCase() == "base64") return true;
            return false;
        })();

        if (isBase64) part.setEncoding('base64');

        // Get the body content of the MIME part
        getContentFromReadableStream(part, (content) => {

            echo("\n--" + boundary);

            // Print the part's headers
            for (let header in part.headers) echo(header + ": " + part.headers[header]);
            
            echo("");
            echo(content);

            // Continue with the next part
            part.resume();

        });

        part.on('error', function (err) {
            console.error(err);
        });

    }).on('close', function () {
        echo("\n--" + boundary + "--");
        // Send the response when reading the form has finished
        res.sendStatus(200);
        console.log('\n^^ MULTIPART POST LOGGED @', new Date(), " ^^\n");
    });

    // Process the incoming request
    form.parse(req);
}

//const echoAndReturn200 = (req, res, next) => echoMultipartPost(req, res, next, () => res.sendStatus(200));


// ==== ROUTING ====

// PM3 gp2gp.ehrRequestHandler call
router.post('/gp2gp/api/gp2gp/ehrRequestHandler', function(req, res, next) {

    return echoMultipartPost(req, res, next);

    //return res.sendStatus(200);

    // Use this call to 'reset' the system, so that the next attachment retrieved is Large
    usedLarge = false;

    const success = returnXmlCallback(res);

    switch (testingScenario) {

        case MOCK_RPC_RESULT.returnExtract_100Small:
            readTextFile("D:\\gp2gp\\large-messaging-bugs\\new\\GP2GP-425\\EHRExtract.txt", success);
            break;

        case MOCK_RPC_RESULT.returnExtract_1Large_3Small:
            readTextFile("D:\\gp2gp\\convo\\G\\1request-response.http", success);
            break;

        case MOCK_RPC_RESULT.returnImportedExtract:
            readTextFile("D:\\gp2gp\\large-message-test\\Imported.xml", success);
            break;
            
            case MOCK_RPC_RESULT.returnExtract_1Absent:
                // readTextFile("D:\\gp2gp\\convo\\H\\1request-response.http", success);
                readTextFile("D:\\gp2gp\\convo\\H\\1request-response-present.http", success);
                break;
                
            case MOCK_RPC_RESULT.returnExtract_1Large:
                //readTextFile("D:\\gp2gp\\convo\\D\\1extract.http", success);
                //readTextFile("D:\\gp2gp\\large-extract\\1extract.http", success);
                //readTextFile("D:\\repos\\spine-message-handler\\target\\_t\\biggyMcBigExtract.xml", success);
                readTextFile("D:\\gp2gp\\large-message-test\\Imported.xml", success);
            break;

        case MOCK_RPC_RESULT.returnExtract_4Small:
            readTextFile("D:\\gp2gp\\convo\\G\\1request-response.http", success);
            break;
            
        default:
            res.sendStatus(500);
            break;
    }

});


let spiderCallCount = 0;

// PM3  calls
router.post('/im/mtrpc', function(req, res, next) {

    //return echoMultipartPost(req, res, next);

    switch (testingScenario) {

        case MOCK_RPC_RESULT.returnExtract_1Large_3Small:
            if (usedLarge) {
                readTextFile("D:\\repos\\spine-message-handler\\app\\test\\assets\\flow2_imDocumentGet_mockResponse.xml", returnXmlCallback(res));
            }
            else {
                readTextFile("D:\\repos\\spine-message-handler\\app\\test\\assets\\flow2_largeBase64.xml", returnXmlCallback(res));
                usedLarge = true;
            }
            break;

        case MOCK_RPC_RESULT.returnExtract_4Small:
            readTextFile("D:\\repos\\spine-message-handler\\app\\test\\assets\\flow2_imDocumentGet_mockResponse.xml", returnXmlCallback(res));
            break;

        case MOCK_RPC_RESULT.returnExtract_1Absent:
            readTextFile("D:\\gp2gp\\convo\\H\\absentattachmentresponse.http", returnXmlCallback(res));
            break;

        case MOCK_RPC_RESULT.returnExtract_1Large:
            readTextFile("D:\\gp2gp\\convo\\H\\presentattachmentresponse.http", returnXmlCallback(res));
            break;

        case MOCK_RPC_RESULT.returnExtract_100Small:
            const open1 = `<im.document.get location="D:\\path\\to\\file.txt" contentType="text/plain" isCompressed="false" gp2gpCompliantFilename="A Name With Spaces.docx">`;
            const close1 = `</im.document.get>`;
            console.debug(++msgCounter);
            return res.status(200).type('application/xml').send(open1 + junk.repeat(msgCounter) + close1);

        case MOCK_RPC_RESULT.returnImportedExtract:
            console.log(req.body);
            const open2 = `<im.document.put id="42" op="insert">`;
            const close2 = `</im.document.put>`;
            return res.status(200).type('application/xml').send(open2 + junk + close2);

        default:
            res.sendStatus(500);
            break;

    }

});

// MMTS call (just echo back what you receive)
router.post('/mmts', echoMultipartPost);

router.post('/test', (req, res, next) => {
    console.log(req.body);
    res.sendStatus(200);
});

module.exports = router;
